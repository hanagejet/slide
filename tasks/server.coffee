'use strict';

gulp = require 'gulp'
$ = require('gulp-load-plugins')()
browserSync = require 'browser-sync'

meta = require '../package.json'
path = meta.gulpvars

gulp.task 'serve', [
  'bower'
  'templates'
  'scripts'
  'styles'
],  ->
  browserSync
    server:
      baseDir: ['.tmp', 'app']
    browser: "google chrome"

  gulp.watch ["#{path['stylesSrc']}/{,**/}*.scss"], ['styles']
  gulp.watch ["#{path['src']}/{,**/}*.jade"], ['templates']
  gulp.watch ["#{path['scriptsSrc']}/*.coffee"], ['scripts']


gulp.task 'serve:dist', ['build'], ->
  browserSync
    server:
      baseDir: 'dist'
    browser: "google chrome"
