'use strict';

gulp = require 'gulp'
$ = require('gulp-load-plugins')()
runSequence = require 'run-sequence'

gulp.task 'clean', ->
  gulp.src ['.tmp', 'dist'],
    read: false
  .pipe $.clean()


gulp.task 'default', ['clean'], (cb) ->
  runSequence('images', 'scripts', 'styles', ['templates'], cb);


gulp.task 'build', ['default'], ->
  jsFilter = $.filter '**/*.js'
  cssFilter = $.filter '**/*.css'

  gulp.src '.tmp/{,**/}*.html'
    .pipe $.plumber $.util.log
    .pipe $.useref.assets searchPath: 'app'
    .pipe jsFilter
    .pipe $.uglify()
    .pipe jsFilter.restore()
    .pipe cssFilter
    .pipe $.csso()
    .pipe cssFilter.restore()
    .pipe $.useref.restore()
    .pipe $.useref()
    .pipe gulp.dest 'dist'
    .pipe $.size()
