path = require 'path'
webpack = require 'webpack'

module.exports = 
  entry:
    animal: './app/scripts/Animal.coffee'
    dog: './app/scripts/Dog.coffee'

  output:
    path: path.join(__dirname, '.tmp')
    publicPath: './.tmp/'
    filename: '[name].js'
    chunkFilename: '[chunkhash].js'

  module:
    loaders:[
      {
        test: /\.coffee$/, loader: 'coffee-loader'
        # test: /\.jade$/, loader: 'jade'
      }
    ]

  resolve:
    root: path.join(__dirname, 'app/scripts')
    # root: path.join(__dirname, 'app/jade')
