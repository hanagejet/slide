'use strict';

gulp = require 'gulp'
$ = require('gulp-load-plugins')()
require('require-dir')('./tasks');

meta = require './package.json'
path = meta.gulpvars


gulp.task 'styles', ->
  scssFileter = $.filter ['pages/*.scss', 'global/*.scss']

  gulp.src ["#{path['stylesSrc']}/{,**/}*.scss"]
    .pipe $.plumber $.util.log
    .pipe scssFileter
    .pipe $.scsslint(
      config: 'scss-lint-config.yml'
    )
    .pipe scssFileter.restore()
    .pipe $.scsslint.reporter()
    .pipe $.sass
      style: 'expanded'
      includePaths: require('node-bourbon').includePaths
    .pipe $.autoprefixer 'last 1 version'
    .pipe gulp.dest "#{path['stylesDist']}"


gulp.task 'images', ->
  gulp.src ["#{path['imagesSrc']}/**/*"]
    .pipe $.plumber $.util.log
    .pipe $.imagemin
      progressive: true
      interlaced: true
    .pipe gulp.dest "#{path['imagesDist']}"
    .pipe $.size()


gulp.task 'templates', ->
  gulp.src ["#{path['src']}/{,**/}*.jade"]
    .pipe $.plumber $.util.log
    .pipe $.ignore.exclude '**/_*.jade'
    .pipe $.jade
      basedir: "#{path['src']}"
      pretty: true
    # .pipe $.beml()
    # .pipe $.webpack(require('./webpack.config.coffee'))
    .pipe gulp.dest "#{path['tmp']}"


gulp.task 'scripts', ->
  gulp.src ["#{path['scriptsSrc']}/*.coffee"]
    .pipe $.plumber $.util.log
    .pipe $.coffeelint()
    .pipe $.coffeelint.reporter()
    .pipe $.coffee()
    # .pipe $.webpack(require('./webpack.config.coffee'))
    .pipe gulp.dest "#{path['scriptsDist']}"
