class T3Slider
  # defaults option
  T3Slider.Defaults = {
    margin: 0

    baseClass: 't3-slider'
    innerElement: 'div'
    itemClass: 't3-item'
  }

  # element widths
  width = {
    inner: 0
    items: null
  }

  # elements dom
  dom = {
    $el: null
    $inner: null
    $items: null
  }

  # item number
  num = {
    next: 0
    prev: 0
    active: 0
  }

  # plugins set
  T3Slider.Plugins = {}

  constructor: (element, options) ->
    @settings = null
    @options = $.extend({}, T3Slider.Defaults, options)
    @dom = $.extend({}, dom)
    @dom.$el = $(element)
    @width = $.extend({}, width)
    @num = $.extend({}, num)
    @_plugins = {}

    @setup()
    @initialize()

    $.each T3Slider.Plugins, $.proxy (key, plugin) ->
      @_plugins[key[0].toLowerCase() + key.slice(1)] = new plugin(this)
    , this

  # スライダー開始
  initialize: ->
    # container にクラスを入れる
    @dom.$el
      .addClass(@settings.baseClass)

    # inner エレメント作る
    @dom.$inner = $('<' + @settings.innerElement + ' class="t3-inner">')

    # inner にitem を格納
    @createInner(@dom.$inner)

    @setItemNum()

    return

  # セッティングのオプションを格納
  setup: ->
    settings = null
    settings = $.extend({}, @options)

    if @settings is null
      @settings = settings

    return

  # inner 要素作成
  createInner: (content) ->
    content.empty()

    @dom.$items =
      @dom.$el
        .children()
        .not('.' + @settings.navContainerClass)
        .addClass(@settings.itemClass)

    # content にitem を代入
    content.append(@dom.$items)

    # container にinner を挿入
    @dom.$el.prepend(@dom.$inner)

    for i in [0 ... @dom.$items.length]

      if i > 0
        @dom.$items.eq(i).css(
          'margin-left': @settings.margin
        )

      @width.inner += @dom.$items.eq(i).outerWidth(true) + 1

    content.width(@width.inner)

    return

  setItemNum: ->
    # 最初の要素にactive 要素をつける
    @dom.$items.eq(0).addClass('active')

    @getItemNum()

  getItemNum: ->
    # number をつける。
    @num.active = @dom.$el.find('.active').index()
    @num.next = @num.active + 1
    @num.prev = @num.active - 1

  # $.fnでプラグインの準備をする
  $.fn.T3Slider = (options) ->
    return @each ->
      if !$(@).data('T3Slider')
        $(@).data('T3Slider', new T3Slider(@, options))

  $.fn.T3Slider.Constructor = T3Slider

class Navigation extends T3Slider
  Navigation.Defaults = {
    nav: false
    navText: ['prev', 'next']
    navContainerElement: 'div'
    navChildElement: 'span'
    navContainerClass: 't3-nav'
    navClass: ['t3-prev', 't3-next']

    dot: false
    dotContainerElement: 'div'
    dotContainerClass: 't3-dots'
    dotElement: 'span'
  }

  # navigation element
  controls = {
    $nav: null
    $prev: null
    $next: null

    $dotContainer: null
  }

  constructor: (slider) ->
    @_core = slider
    @$el = @_core.dom.$el
    @_controls = $.extend({}, controls)
    @_count = 0
    @_position = 0

    @_core.options = $.extend({}, Navigation.Defaults, @._core.options)

    @initialize()

  initialize: ->
    options = @_core.options

    # nav 表示判別
    if options.nav
      @nav()

    # dot 表示判別
    if options.dot
      @dots()

    # containerにナビゲーションを挿入
    @$el.append(@_controls.$nav)

    # containerにドットナビを挿入
    @$el.append(@_controls.$dotContainer)

    return

  dots: ->
    options = @_core.options

    # dot ナビゲーションを作成
    @_controls.$dotContainer =
      $('<' + options.dotContainerElement + '>')
      .addClass(options.dotContainerClass)

    for i in [0 ... @_core.dom.$items.length]
      @_controls.$dotContainer.append('<' + options.dotElement + '>')

    @_controls.$dotContainer.children().first().addClass('active')
    @_controls.$dotContainer.children()
      .on 'click', $.proxy (e) ->
        $(e.target).parent().children().removeClass('active')
        index = $(e.target).addClass('active').index()
        @_core.num.active = index
        @setItemNum()

        @_position +=
          Math.ceil(@_core.dom.$items.eq(@_core.num.active).offset().left)
        @_core.dom.$inner.css
          'transform': 'translateX(' + -@_position + 'px)'
        return
      , this

    return

  nav: ->
    options = @_core.options

    # container を作成
    @_controls.$nav = $('<' + options.navContainerElement + '>')
    @_controls.$nav.addClass(options.navContainerClass)

    # prev, next を作成
    @_controls.$prev = $('<' + options.navChildElement + '>')
    @_controls.$next = @_controls.$prev.clone()

    @_controls.$prev
      .html(options.navText[0])
      .addClass(options.navClass[0])
      .prependTo(@_controls.$nav)
      .on 'click', $.proxy (e) ->
        @prev()

        return
      , this

    @_controls.$next
      .html(options.navText[1])
      .addClass(options.navClass[1])
      .appendTo(@_controls.$nav)
      .on 'click', $.proxy (e) ->
        @next()

        return
      , this

    return

  prev: ->
    @getPosition(false)

    return

  next: ->
    @getPosition(true)

    return

  getPosition: (flag) ->
    if flag
      @_core.num.active++

      # 最後までいったときはreturn
      if @_core.num.active > @_core.dom.$items.length - 1
        @_core.num.active = @_core.dom.$items.length - 1
        return

      @.setItemNum()
      @_position +=
        @_core.dom.$items.eq(@_core.num.prev).width() + @_core.options.margin
    else
      @_core.num.active--

      # 最初から戻ろうとした時はreturn
      if @_core.num.active < 0
        @_core.num.active = 0
        return

      @.setItemNum()
      @_position -=
        @_core.dom.$items.eq(@_core.num.active).width() + @_core.options.margin

    if @_core.options.dot
      @_controls.$dotContainer.children().removeClass('active')
      @_controls.$dotContainer.children().eq(@_core.num.active).addClass('active')

    @_core.dom.$inner.css
      'transform': 'translateX(' + -@_position + 'px)'

    return

  setItemNum: ->
    @_core.dom.$items.removeClass('active')
    @_core.dom.$items.eq(@_core.num.active).addClass('active')
    @_core.getItemNum()

    return

  $.fn.T3Slider.Constructor.Plugins.Navigation = Navigation
